const routes = [
  {
    path: "/",
    component: () => import("layouts/MainLayout.vue"),
    children: [
      { path: "", component: () => import("pages/Index.vue") },
      {
        path: "/merchant",
        component: () => import("pages/Merchant.vue"),
        meta: {
          requiresAuth: true
        }
      },
      {
        path: "/userprofile",
        component: () => import("pages/UserPage.vue"),
        meta: {
          requiresAuth: true
        }
      },
      {
        path: "/shop",
        component: () => import("pages/ShopsPage.vue"),
        meta: {
          requiresAuth: true
        }
      },
      {
        path: "/product",
        component: () => import("pages/Product.vue"),
        meta: {
          requiresAuth: true
        }
      },
      {
        path: "/cart",
        component: () => import("pages/CartPage.vue"),
        meta: {
          requiresAuth: true
        }
      },
      {
        path: "/list",
        component: () => import("pages/ListPage.vue"),
        meta: {
          requiresAuth: true
        }
      },
      {
        path: "/shopsubscribe",
        component: () => import("pages/ShopInSub.vue"),
        meta: {
          requiresAuth: true
        }
      },
      {
        path: "/typechildren",
        component: () => import("pages/TypeChildren.vue"),
        meta: {
          requiresAuth: true
        }
      },
      {
        path: "/typemen",
        component: () => import("pages/TypeMen.vue"),
        meta: {
          requiresAuth: true
        }
      },
      {
        path: "/typewomen",
        component: () => import("pages/TypeWomen.vue"),
        meta: {
          requiresAuth: true
        }
      },
      {
        path: "/typeshoes",
        component: () => import("pages/TypeShoes.vue"),
        meta: {
          requiresAuth: true
        }
      },
      {
        path: "/typewatches",
        component: () => import("pages/TypeWatches.vue"),
        meta: {
          requiresAuth: true
        }
      },
      {
        path: "/typedressing",
        component: () => import("pages/TypeDressing.vue"),
        meta: {
          requiresAuth: true
        }
      },
      {
        path: "/typeswimwear",
        component: () => import("pages/TypeSwimwear.vue"),
        meta: {
          requiresAuth: true
        }
      },
      {
        path: "/orders",
        component: () => import("pages/OrdersPage.vue"),
        meta: {
          requiresAuth: true
        }
      }
    ]
  },
  {
    path: "*",
    component: () => import("pages/Error404.vue")
  },
  {
    path: "/admin",
    component: () => import("pages/AdminPage.vue")
  }
];

export default routes;
